# Cypress Demo: Cypress, Cucumber & POM
## 1. Setup Project
Step 1: Create a folder and Generate package.json

> 1. Create a new folder, I called mine project cypress_cucumber
> 2. Type > npm init (package.json) is created


Step 2: Install Cypress

> Run > npm install cypress --save-dev


Step 3: Install Cucumber

> Run > npm install --save-dev cypress-cucumber-preprocessor


Step 4: Add below code snippet in "cypress.config.js"
```
const { defineConfig } = require("cypress");
const cucumber = require("cypress-cucumber-preprocessor").default;
module.exports = defineConfig({
  e2e: {
    setupNodeEvents(on, config) {
      on("file:preprocessor", cucumber());
    },
  },
});
```


Step 5: Add below code snippet in "package.json"

```
"cypress-cucumber-preprocessor": {
    "nonGlobalStepDefinitions": false
 }
```


Step 6: Add below code snippet in "package.json"
```
"step_definitions": "cypress/e2e/cucumber/Tests"
```


Step 7: Add the below line in "cypress.config.js" to run "*.feature" file
```
specPattern: "**/*.feature",
```


## 2. Run Cypress
Step 1: Open the Cypress test runner with the following command
> npm run test