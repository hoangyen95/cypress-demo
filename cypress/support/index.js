// cypress/support/index.js

Cypress.on('uncaught:exception', (err, runnable) => {
    // Log the error to the console or your logging service
    console.error('Uncaught Exception:', err.message)
  
    // Optionally, prevent Cypress from failing the test immediately
    return false
  })
  